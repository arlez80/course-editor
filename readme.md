# Course Editor for Godot Engine

A course editor for Godot Engine.
[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/E1E44AWTA)

## Video

https://www.youtube.com/watch?v=pL6JegLaiso
https://www.youtube.com/watch?v=1SWaZ_51njk

## How to use (Basic)

+ Inherit course_gen/Course.tscn
+ Put the course_gen/point/WayPoint.tscn under the Path-node (Spatial)

## How to use (Advanced)

+ Make a shader for course
+ Append features to Course.gd
+ etc...

## Shader Hint

* UV: Normal UV
* UV2: Surface type (Wall, Breakdown lane, Road) Compressed

## Not TODO

+ Translation all comments

## License

MIT License

## Author

あるる / きのもと 結衣 @arlez80
