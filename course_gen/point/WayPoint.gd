extends Spatial

export(float) var road_width:float = 6.0
export(float) var left_edge_width:float = 3.8
export(float) var right_edge_width:float = 3.8
export(float) var left_wall_height:float = 1.0
export(float) var right_wall_height:float = 1.0
export(float) var road_graphic_type:float = 0.0

export(PackedScene) var item_pattern:PackedScene = null
