"""
	コース自動生成プログラム by あるる（きのもと 結衣）

	MIT License
"""

tool

extends Spatial

class WayPoint:
	var spatial:Spatial
	var proper_transform:Transform
	var position:Vector3
	var prev:WayPoint
	var next:WayPoint
	var parcentage:float = 0.0

const default_curve_distance:float = 5.0
const debug_curve_distance:float = 15.0
const course_fence_height:float = 1.0

export(float) var course_fence_collision_height:float = 5.0

var start_way_point:WayPoint = null
var way_points:Array = []
var curve_distance:float = 5.0

var road_collision_shape:Array = []
var edge_collision_shape:Array = []
var wall_collision_shape:Array = []

func _ready( ):
	self._generate( )

"""
	コース生産
"""
func _generate( ):
	self._fetch_all_way_points( )

	if Engine.editor_hint:
		return

	self.curve_distance = self.default_curve_distance

	$MeshInstance.mesh = self._create_all_route_meshes( )

	self._create_all_way_point_boards( )

	$RoadCollision/CollisionShape.shape = ConcavePolygonShape.new( )
	$RoadCollision/CollisionShape.shape.set_faces( PoolVector3Array( self.road_collision_shape ) )

	$EdgeCollision/CollisionShape.shape = ConcavePolygonShape.new( )
	$EdgeCollision/CollisionShape.shape.set_faces( PoolVector3Array( self.edge_collision_shape ) )

	$WallCollision/CollisionShape.shape = ConcavePolygonShape.new( )
	$WallCollision/CollisionShape.shape.set_faces( PoolVector3Array( self.wall_collision_shape ) )

"""
	全てのウェイポイントを取得する
"""
func _fetch_all_way_points( ):
	self.way_points.clear( )

	for t in $Path.get_children( ):
		var wp: = WayPoint.new( )
		wp.position = t.global_transform.origin
		wp.spatial = t
		self.way_points.append( wp )

	for i in range( self.way_points.size( ) ):
		var a:WayPoint = self.way_points[i-1]
		var b:WayPoint = self.way_points[i]

		a.next = b
		b.prev = a
		b.parcentage = float( i ) / self.way_points.size( )

	for t in self.way_points:
		var tf:Transform = t.spatial.global_transform
		var ptf:Transform = Transform( )
		ptf.origin = t.position
		ptf = ptf.looking_at( t.next.position, tf.basis.y )
		ptf.basis = ptf.basis.rotated( ptf.basis.y, PI )
		t.proper_transform = ptf

	if 0 < self.way_points.size( ):
		self.start_way_point = self.way_points[0]

"""
	メッシュ生成
"""
func _create_all_route_meshes( put_item_pattern:bool = true ) -> Mesh:
	var st: = SurfaceTool.new( )
	var vid:int = 0
	st.begin( Mesh.PRIMITIVE_TRIANGLES )

	self.road_collision_shape.clear( )
	self.edge_collision_shape.clear( )
	self.wall_collision_shape.clear( )

	for wp in self.way_points:
		vid = self._create_route_meshes( st, vid, wp.prev, wp, wp.next, wp.next.next )
		if put_item_pattern:
			if wp.spatial.item_pattern != null:
				var ip = wp.spatial.item_pattern.instance( )
				ip.transform = wp.spatial.transform
				self.add_child( ip )

	return st.commit(
		null,
		Mesh.ARRAY_FORMAT_VERTEX
	|	Mesh.ARRAY_FORMAT_NORMAL
	|	Mesh.ARRAY_FORMAT_TEX_UV	# グラフィック用
	|	Mesh.ARRAY_FORMAT_TEX_UV2	# テクスチャ選択用
	|	Mesh.ARRAY_FORMAT_INDEX
	|	Mesh.ARRAY_COMPRESS_NORMAL
	|	Mesh.ARRAY_COMPRESS_TEX_UV2
	)

func _create_route_meshes( st:SurfaceTool, vid:int, prev_start:WayPoint, start:WayPoint, end:WayPoint, next_end:WayPoint ) -> int:
	var distance:float = start.position.distance_to( end.position )
	if distance == 0.0:
		#print( "SKIP!!", start.spatial.global_transform, end.spatial.global_transform )
		return vid

	var start_vid:int = vid
	var collision_vertices:Array = []

	var steps:int = ceil( distance / curve_distance )
	for i in range( steps + 1 ):
		var t:float = float( i ) / steps
		var s:float = 1.0 - t
		var tf:Transform = self._calc_curve( prev_start, start, end, next_end, t )
		var road_width:float = lerp( start.spatial.road_width, end.spatial.road_width, t )
		var left_edge_width:float = lerp( start.spatial.left_edge_width, end.spatial.left_edge_width, t )
		var right_edge_width:float = lerp( start.spatial.right_edge_width, end.spatial.right_edge_width, t )
		var left_wall_height:float = lerp( start.spatial.left_wall_height, end.spatial.left_wall_height, t )
		var right_wall_height:float = lerp( start.spatial.right_wall_height, end.spatial.right_wall_height, t )
		var road_graphic_type:float = lerp( start.spatial.road_graphic_type, end.spatial.road_graphic_type, t )

		# 6点分追加
		# 左壁上　左壁下　道左　道右　右壁下　右壁上
		st.add_normal( tf.basis.x )
		st.add_uv( Vector2( 0, i ) )
		st.add_uv2( Vector2( 1, road_graphic_type ) )
		st.add_vertex( tf.origin + tf.basis.y * right_wall_height + ( tf.basis.x * ( - road_width - right_edge_width ) ) )

		st.add_normal( tf.basis.x )
		st.add_uv( Vector2( 1, i ) )
		st.add_uv2( Vector2( 0.5, road_graphic_type ) )
		st.add_vertex( tf.origin + ( tf.basis.x * ( - road_width - right_edge_width ) ) )

		st.add_normal( tf.basis.y )
		st.add_uv( Vector2( 0, i ) )
		st.add_uv2( Vector2( 0, road_graphic_type ) )
		st.add_vertex( tf.origin + ( tf.basis.x * ( - road_width ) ) )

		st.add_normal( tf.basis.y )
		st.add_uv( Vector2( 1, i ) )
		st.add_uv2( Vector2( 0, road_graphic_type ) )
		st.add_vertex( tf.origin + ( tf.basis.x * ( + road_width ) ) )

		st.add_normal( -tf.basis.x )
		st.add_uv( Vector2( 0, i ) )
		st.add_uv2( Vector2( 0.5, road_graphic_type ) )
		st.add_vertex( tf.origin + ( tf.basis.x * ( + road_width + left_edge_width ) ) )

		st.add_normal( -tf.basis.x )
		st.add_uv( Vector2( 1, i ) )
		st.add_uv2( Vector2( 1, road_graphic_type ) )
		st.add_vertex( tf.origin + tf.basis.y * left_wall_height + ( tf.basis.x * ( + road_width + left_edge_width ) ) )

		collision_vertices += [
			tf.origin + tf.basis.y * course_fence_collision_height + ( tf.basis.x * ( - road_width - right_edge_width ) )
		,	tf.origin + ( tf.basis.x * ( - road_width - right_edge_width ) )
		,	tf.origin + ( tf.basis.x * ( - road_width ) )
		,	tf.origin + ( tf.basis.x * ( + road_width ) )
		,	tf.origin + ( tf.basis.x * ( + road_width + left_edge_width ) )
		,	tf.origin + tf.basis.y * course_fence_collision_height + ( tf.basis.x * ( + road_width + left_edge_width ) )
		]

		vid += 6

	var col_vid:int = 0

	for i in range( steps ):
		self.road_collision_shape.append( collision_vertices[col_vid] )
		self.road_collision_shape.append( collision_vertices[col_vid+5] )
		self.road_collision_shape.append( collision_vertices[col_vid+6] )
		self.road_collision_shape.append( collision_vertices[col_vid+5] )
		self.road_collision_shape.append( collision_vertices[col_vid+6] )
		self.road_collision_shape.append( collision_vertices[col_vid+11] )

		for k in range( 5 ):
			# ポリゴン
			st.add_index( start_vid )
			st.add_index( start_vid + 1 )
			st.add_index( start_vid + 6 )

			st.add_index( start_vid + 1 )
			st.add_index( start_vid + 7 )
			st.add_index( start_vid + 6 )

			# 当たり判定
			var target:Array = []
			match k:
				0,4:
					target = self.wall_collision_shape
				1,3:
					target = self.edge_collision_shape
				2:
					target = self.road_collision_shape

			target.append( collision_vertices[col_vid] )
			target.append( collision_vertices[col_vid + 1] )
			target.append( collision_vertices[col_vid + 6] )
			target.append( collision_vertices[col_vid + 1] )
			target.append( collision_vertices[col_vid + 7] )
			target.append( collision_vertices[col_vid + 6] )

			start_vid += 1
			col_vid += 1

		start_vid += 1
		col_vid += 1

	return vid

"""
	ウェイポイント間の補間を計算
"""
func _calc_curve( pre_start:WayPoint, start:WayPoint, end:WayPoint, next_end:WayPoint, t:float ) -> Transform:
	var pre_start_tf:Transform = pre_start.spatial.global_transform
	var start_tf:Transform = start.spatial.global_transform
	var end_tf:Transform = end.spatial.global_transform
	var next_end_tf:Transform = next_end.spatial.global_transform

	return Transform(
		start_tf.basis.slerp( end_tf.basis, t )
	,	start_tf.origin.cubic_interpolate( end_tf.origin, pre_start_tf.origin, next_end_tf.origin, t )
	)

"""
	ウェイポイント間の補間を計算
	（向き正確）
"""
func _calc_curve_with_proper_transform( pre_start:WayPoint, start:WayPoint, end:WayPoint, next_end:WayPoint, t:float ) -> Transform:
	var pre_start_tf:Transform = pre_start.proper_transform
	var start_tf:Transform = start.proper_transform
	var end_tf:Transform = end.proper_transform
	var next_end_tf:Transform = next_end.proper_transform

	return Transform(
		start_tf.basis.slerp( end_tf.basis, t )
	,	start_tf.origin.cubic_interpolate( end_tf.origin, pre_start_tf.origin, next_end_tf.origin, t )
	)

"""
	ウェイポイントボードを置く
"""
func _create_all_way_point_boards( ):
	var id:int = 0

	for wp in self.way_points:
		var board:StaticBody = preload("point/WayPointBoard.tscn").instance( )
		var tf:Transform = wp.spatial.transform
		var f:float = (
			wp.spatial.left_edge_width
		+	wp.spatial.right_edge_width
		+	wp.spatial.road_width
		)
		board.way_point_id = id
		board.way_point = wp
		board.transform.basis = tf.basis
		board.transform.basis.x *= f
		board.transform.basis.y *= 8.0
		board.transform.origin = tf.origin + ( tf.basis.x * ( wp.spatial.right_edge_width - wp.spatial.left_edge_width ) ) + ( tf.basis.y * 4.0 )
		$WayPointBoards.add_child( board )
		id += 1

"""
	毎フレーム（エディタデバッグ用のみ使用する）
"""
func _process( delta:float ):
	if Engine.editor_hint:
		self._debug_draw( )

"""
	デバッグ表示
"""
func _debug_draw( ):
	var debug_mesh_instance:MeshInstance = null

	for t in self.get_tree( ).get_nodes_in_group( "_debug:arlez80:course" ):
		if t is MeshInstance:
			debug_mesh_instance = t
			break

	if debug_mesh_instance == null:
		debug_mesh_instance = preload("DebugMeshInstance.tscn").instance( PackedScene.GEN_EDIT_STATE_MAIN )
		self.add_child( debug_mesh_instance )

	debug_mesh_instance.material_override = $MeshInstance.material_override

	self.curve_distance = self.debug_curve_distance
	self._fetch_all_way_points( )
	if self.way_points.size( ) < 3:
		debug_mesh_instance.mesh = null
		return

	debug_mesh_instance.mesh = self._create_all_route_meshes( false )
